#include <iostream>
#include "tinyxml2.h" 
#include <conio.h>
#include <fstream>


using namespace std;
using namespace tinyxml2;

int main()
{
	XMLDocument doc;
	bool loadOkay = doc.LoadFile("example.xml");

	if (loadOkay)
	{
		printf("open success\n");
	}
	else
	{
		printf("Failed to load file \n");
	}

	//root node access
	XMLElement* pRoot = doc.FirstChildElement("LibraryIndex");
	if (!pRoot) return 0;

	//access "welcom" element to get value
	XMLElement* pElem = pRoot->FirstChildElement("Messages")->FirstChildElement("Welcome");
	if (!pElem) return 0;

	//get element value (Welcome).
	char* pszNode = (char*)pElem->Value();
	if (pszNode)
		cout << "NODE" << pszNode;

	//get element text(Welcome to MyApp)
	char* pszText = (char*)pElem->GetText();
	if (pszText)
		printf("Text: %s\n", pszText);



	_getch();
}
